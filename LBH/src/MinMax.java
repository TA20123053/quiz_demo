import java.util.Arrays;

public class MinMax {

	public static void main(String[] args) {
		int arr[]= {2,9,6,8};
		Arrays.sort(arr);
		int min=arr[0];
		for(int i=0;i<arr.length;i++) {
			arr[i]=arr[i]-min;
			arr[i]=arr[i]*min;
		}
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i]+",");
		}
	}

}
