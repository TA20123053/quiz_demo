/*given a string take the input2's first digit as the word and 2nd digit as position to reverse
 * the string. Print the string in a reverse order in the first half and the 2nd half as it is.
 * print the 2nd half first and the reversed first half second. If the length is odd then repeat 
 * the middle word in both the halves. */ 
public class String1 {

	public static void main(String[] args) {
		String input1="Today is a nice day";
		StringBuilder str = new StringBuilder();
		int input2=41;
		int n1=(input2/10)%10;
		int n2=input2%10;
		String s[]=input1.split(" ");
		int len=input1.length();
		String str1=s[n1-1];
		int len1=str1.length();
		int len2=len1/2;
		String str12,str13,S;
		if(len1%2==0) {
			str12=str1.substring(0,len2);
			str.append(str12);
			str.reverse();
			str13=str1.substring(len2,len1);
			S=str13+str;
		}
		else {
			str12=str1.substring(0,len2+1);
			str.append(str12);
			str.reverse();
			str13=str1.substring(len2,len1);
			S=str13+str;
		}
		System.out.println("String: "+S);
	}

}
