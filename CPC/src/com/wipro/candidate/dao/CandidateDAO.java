package com.wipro.candidate.dao;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



import com.wipro.candidate.bean.CandidateBean;
import com.wipro.candidate.util.DBUtil;


public class CandidateDAO {
	public String addCandidate(CandidateBean studentBean)
	{
			String status="";
			int n=0;
			String sql="insert into CANDIDATE_TBL values(?,?,?,?,?,?,?)";
			try {
				PreparedStatement ps=DBUtil.getDBConn().prepareStatement(sql);
				ps.setString(1, studentBean.getId());
				ps.setString(2, studentBean.getName());
				ps.setInt(3, studentBean.getM1());
				ps.setInt(4, studentBean.getM2());
				ps.setInt(5, studentBean.getM3());
				ps.setString(6, studentBean.getResult());
				ps.setString(7, studentBean.getGrade());
				n=ps.executeUpdate();
				if(n>=1) {
					status="SUCCESS";
				}
				else
					status="FAIL";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return status;
	}
	public ArrayList<CandidateBean> getByResult(String criteria)
	{
		ArrayList<CandidateBean> list=new ArrayList<CandidateBean>();
		String sql="";
		ResultSet rs;
		CandidateBean cb= new CandidateBean();
		switch (criteria) {
		case "PASS":
			sql="select * from CANDIDATE_TBL where result like 'PASS'";
			break;
		case "FAIL":
			sql="select * from CANDIDATE_TBL where result like 'FAIL'";
			break;
		case "ALL":
			sql="select * from CANDIDATE_TBL where result like 'ALL'";
			break;
		default:
			list=null;
			break;
		}
		try {
			PreparedStatement ps= DBUtil.getDBConn().prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next()) {
				cb.setId(rs.getString(1));
				cb.setName(rs.getString(2));
				cb.setM1(rs.getInt(3));
				cb.setM2(rs.getInt(4));
				cb.setM3(rs.getInt(5));
				cb.setResult(rs.getString(6));
				cb.setGrade(rs.getString(7));
				list.add(cb);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list=null;
		}
		return list;
	}
	public String generateCandidateId (String name)
	{
		String id="";
		String sql="select CANDID_SEQ.nextval from dual";
		ResultSet rs;
		int n=0;
		try {
			name = name.toUpperCase();
			PreparedStatement ps=DBUtil.getDBConn().prepareStatement(sql);
			rs=ps.executeQuery();
			while(rs.next())
			{
				n = rs.getInt(1);
			}
			id=name.substring(0,2)+String.valueOf(n);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
}
