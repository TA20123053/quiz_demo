package com.wipro.candidate.service;

import java.util.ArrayList;


import com.wipro.candidate.bean.CandidateBean;
import com.wipro.candidate.dao.CandidateDAO;
import com.wipro.candidate.util.WrongDataException;


public class CandidateMain {

	/**
	 * @param args
	 */
	public String addCandidate(CandidateBean studBean)
	{
		String result="";
		CandidateDAO dao=new CandidateDAO();
		try {
		
	  if(studBean==null || studBean.getName()==null || studBean.getName().length()<2 || 
	  studBean.getM1()<0 || studBean.getM1()>100 || studBean.getM2()<0 || studBean.getM2()>100
	  || studBean.getM3()<0 || studBean.getM3()>100) {
			throw new WrongDataException();
		}
	  else {
		  studBean.setId(dao.generateCandidateId(studBean.getName()));
		  int m1=studBean.getM1();
		  int m2=studBean.getM2();
		  int m3=studBean.getM3();
		  int total=m1+m2+m3;
		  if(total>=240) {
			  studBean.setResult("PASS");
			  studBean.setGrade("Distinction");
		  }
		  else if(total>=  180 && total<240) {
			  studBean.setResult("PASS");
			  studBean.setGrade("First Class");
		  }
		  else if(total>= 150 && total<180) {
			  studBean.setResult("PASS");
			  studBean.setGrade("Second Class");
		  }
		  else if(total>= 105 && total<150) {
			  studBean.setResult("PASS");
			  studBean.setGrade("Third Class");
		  }
		  else if(total<105) {
			  studBean.setResult("FAIL");
			  studBean.setGrade("No Grade");
		  }
		  String s=dao.addCandidate(studBean);
		  if(s.equals("SUCCESS")) {
			  result=studBean.getId()+":"+studBean.getResult();
		  }
		  else
		  {
			  result="Error";
		  }
	  }
	} catch (WrongDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=e.toString();
		  }
	    return result;
		
	}
	public ArrayList<CandidateBean> displayAll(String criteria)
	{
		CandidateDAO dao=new CandidateDAO();
		ArrayList<CandidateBean> list=new ArrayList<CandidateBean>();
		if(criteria.equals("PASS")||criteria.equals("FAIL")||criteria.equals("ALL"))
		{
			list =dao.getByResult(criteria);
			return list;
		}
		else
		{
			try
			{
				throw new WrongDataException();
				
			}
			catch (Exception e) {
				// TODO: handle exception
				return null;
			}
		}
		}
	public static void main(String[] args) {
		//write code here
	}

}
